# vue-trivia-game

## The best trivia game you will ever find

##### by Håkon Dalen Hestnes and Michael Mellemseter

## About

A Trivia game web application made using Vue and the open trivia db API. The application lets the user start a trivia game, which will ask the user then questions. The user will then 
see the results of the trivia game, with feedback on the result, and options to either start a new game or to go to main menu. 

The application uses Vuex to manage state.

## How to run

- Clone the project
- Go to the root folder of the project
- Run `npm install`
- Run `npm run serve`

## Code design/structure
When the user presses start game, the questions are fetched from the API and put into the store. The screen displaying the question then accesses these questions, and sets values
depending on the user's answers. When the game is finished, and the results are shown, these values are accessed in the store, and displayed. If the user chooses to play again, then new questions are fetched from the API and put in the store. 

Most of the code can be found in the folder *src*. This folder is divided into the subfolders *api*, *assets*, *components*, *css* and *store*.

**1. api** <br />
This folder contains one file, *questionAPI.js*. This file is responsible for fetching the data from the API. 

**2. assets** <br />
This folder contains static files that are not code, but is used throughout the project. In this case, font files.

**3. components** <br />
This folder contains all the Vue components. The folder contains the subfolders *StartPage*, *Question* and *Result*. 

**4. css** <br />
This folder contains all the CSS files for the project. These files are then imported into the main css file *src/main.css*.

**5. store** <br />
This folder contains one file, index.js, which is the the file that handles the state of the project - using Vuex. 
 
