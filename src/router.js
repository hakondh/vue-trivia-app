import VueRouter from 'vue-router'
import StartPage from './components/StartPage/StartPage'
import Question from "./components/Questions/Question"
import Result from './components/Result/Result'

const routes = [
    {
        path: '/',
        component: StartPage,
    },
    {
        path: '/questions/:questionNumber',
        component: Question
    },
    {
        path: '/result',
        component: Result
    }
]

const router = new VueRouter({routes})

export default router;