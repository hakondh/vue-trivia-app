import Vue from 'vue'
import Vuex from 'vuex'
import { fetchQuestions } from "@/api/questionAPI";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        questions: [],
        answers: [],
        results: []
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setAnswers: (state, payload) => {
            state.answers = payload
        },
        setResults: (state, payload) => {
            state.results = payload
        }
    },
    actions: {
        fetchQuestions({commit}) {
            return new Promise((resolve, reject) => {
                fetchQuestions().then(questions => {
                    commit('setQuestions', questions.results)
                    resolve()
                })
            })

        }
    },
    getters: {
        // Get the points the user got
        points: (state) => {
            return state.results.filter(result => result).length * 10
        },
        // Get the questions
        questions: (state) => {
            return state.questions.map(question => question.question)
        },
        // Get the correct answers to the questions
        correctAnswers: (state) => {
            return state.questions.map(question => question.correct_answer)
        }
    }
})